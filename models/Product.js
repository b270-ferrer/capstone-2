///////////////////////////////////////////////////
//              SESSION 42                       //
//          - DATA MODEL DESIGN                  //
//          - USER REGISTRATION                  //
//          - USER AUTHENTICATION                //
///////////////////////////////////////////////////


// Product Model Requirement

// Product 
// 	- Name (string)
// 	- Description (string)
// 	- Price (Number)
//	- createdOn (Data- default to current timestamp)



const mongoose = require("mongoose");

const productSchema = new mongoose.Schema ({
	name:               {       type: String,   required: [true, "product name is required"]		},
	description:        {       type: String,   required: [true, "product description is required"]	},
	price:              {       type: Number,   default: 0											},
	quantity:           {       type: Number,   default: 0											},
	imageUrl:         	{       type: [String], default: []											},	
	isActive: 			{       type: Boolean,  default: true										},
	createdOn:          {       type: Date,     default: new Date()									}		
});

module.exports = mongoose.model("Product", productSchema);