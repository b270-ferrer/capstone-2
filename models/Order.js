///////////////////////////////////////////////////
//              SESSION 42                       //
//          - DATA MODEL DESIGN                  //
//          - USER REGISTRATION                  //
//          - USER AUTHENTICATION                //
///////////////////////////////////////////////////



const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema ({

	userId: 			{		type: Object,	default: null									},
	userName: 			{		type: String,	required: [true, "User Name is required"]		},
	products: [{	
		id:   			{		type: Object,	default: null									},
		name: 			{		type: String,	required: [true, "Product Name is required"]	},
		price:  		{		type: Number,	required: [true, "Product Price is required"]	},
		quantity: 		{		type: Number,	required: [true, "Product Quantity is required"]},
		total: 			{		type: Number,	required: [true, "Product Amount is required"]	},
	}],					
	totalAmount: 		{		type: Number,	default: 0										},
	purchaseOn: 		{		type: Date,		default:new Date()								},	
	
});





module.exports = mongoose.model("Order", orderSchema);