///////////////////////////////////////////////////
//              SESSION 42                       //
//          - DATA MODEL DESIGN                  //
//          - USER REGISTRATION                  //
//          - USER AUTHENTICATION                //
///////////////////////////////////////////////////


// User Model Requirement

// User 
// 	- email (string)
// 	- password (string)
// 	- isAdmin (Boolean default to false)



const mongoose = require("mongoose");

const userSchema = new mongoose.Schema ({
	name: 				{		type: String,	required: [true, "First Name is required"]		},
	email: 				{		type: String,	required: [true, "Email is required"]			},
	password: 			{		type: String,	required: [true, "Password is required"]		},
	isAdmin: 			{		type: Boolean,	default: false									},
	mobileNo: 			{		type: String,	required: [true, "Mobile Number is required"]	},
	orders: [{
		orderId: 		{		type: Object,	default: null									}
	}],
	cart:{
		products: [{	
			id:   		{		type: Object,	default: null									},
			name: 		{		type: String,	default: ""										},
			price:  	{		type: Number,	default: 0										},
			quantity: 	{		type: Number,	default: 0										},
			total: 		{		type: Number,	default: 0										},
		}],
		totalAmount:  	{		type: Number,	default: 0										}
	}
});

module.exports = mongoose.model("User", userSchema);