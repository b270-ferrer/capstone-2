/*
		$ npm init -y
		Wrote to C:\Users\charl\Documents\b270\capstone2\package.json:

		{
		  "name": "capstone2",
		  "version": "1.0.0",
		  "description": "",
		  "main": "index.js",
		  "scripts": {
		    "test": "echo \"Error: no test specified\" && exit 1"
		  },
		  "keywords": [],
		  "author": "",
		  "license": "ISC"
		}
		//////////////////////////////////////////////////
		$ npm i express
		npm notice created a lockfile as package-lock.json. You should commit this file.
		npm WARN capstone2@1.0.0 No description
		npm WARN capstone2@1.0.0 No repository field.

		+ express@4.18.2
		added 57 packages from 42 contributors and audited 57 packages in 5.598s

		7 packages are looking for funding
		  run `npm fund` for details

		found 0 vulnerabilities
		//////////////////////////////////////////////////////
		$ npm i mongoose
		npm WARN notsup Unsupported engine for bson@5.2.0: wanted: {"node":">=14.20.1"} (current: {"node":"14.18.0","npm":"6.14.15"})
		npm WARN notsup Not compatible with your version of node/npm: bson@5.2.0
		npm WARN notsup Unsupported engine for mongodb@5.3.0: wanted: {"node":">=14.20.1"} (current: {"node":"14.18.0","npm":"6.14.15"})
		npm WARN notsup Not compatible with your version of node/npm: mongodb@5.3.0
		npm WARN capstone2@1.0.0 No description
		npm WARN capstone2@1.0.0 No repository field.

		+ mongoose@7.1.0
		added 24 packages from 64 contributors and audited 81 packages in 8.79s

		8 packages are looking for funding
		  run `npm fund` for details

		found 0 vulnerabilities
		///////////////////////////////////////////////////////
		$ npm i mongoose
		npm WARN notsup Unsupported engine for bson@5.2.0: wanted: {"node":">=14.20.1"} (current: {"node":"14.18.0","npm":"6.14.15"})
		npm WARN notsup Not compatible with your version of node/npm: bson@5.2.0
		npm WARN notsup Unsupported engine for mongodb@5.3.0: wanted: {"node":">=14.20.1"} (current: {"node":"14.18.0","npm":"6.14.15"})
		npm WARN notsup Not compatible with your version of node/npm: mongodb@5.3.0
		npm WARN capstone2@1.0.0 No description
		npm WARN capstone2@1.0.0 No repository field.

		+ mongoose@7.1.0
		added 24 packages from 64 contributors and audited 81 packages in 8.79s

		8 packages are looking for funding
		  run `npm fund` for details

		found 0 vulnerabilities
		///////////////////////////////////////////////////////
		$ npm install bcrypt

		> bcrypt@5.1.0 install C:\Users\charl\Documents\b270\capstone2\node_modules\bcrypt
		> node-pre-gyp install --fallback-to-build

		[bcrypt] Success: "C:\Users\charl\Documents\b270\capstone2\node_modules\bcrypt\lib\binding\napi-v3\bcrypt_lib.node" is installed via remote
		npm WARN capstone2@1.0.0 No description
		npm WARN capstone2@1.0.0 No repository field.

		+ bcrypt@5.1.0
		added 60 packages from 135 contributors and audited 141 packages in 14.4s

		11 packages are looking for funding
		  run `npm fund` for details

		found 0 vulnerabilities
		/////////////////////////////////////////////////////
		$ npm install jsonwebtoken
		npm WARN capstone2@1.0.0 No description
		npm WARN capstone2@1.0.0 No repository field.

		+ jsonwebtoken@9.0.0
		added 7 packages from 7 contributors and audited 148 packages in 5.135s

		11 packages are looking for funding
		  run `npm fund` for details

		found 0 vulnerabilities
		///////////////////////////////////////////////////////
		$ npm install cors
		npm WARN capstone2@1.0.0 No description
		npm WARN capstone2@1.0.0 No repository field.

		+ cors@2.8.5
		added 1 package from 1 contributor and audited 149 packages in 1.955s

		11 packages are looking for funding
		  run `npm fund` for details

		found 0 vulnerabilities
		/////////////////////////////////////////////////////

		nodemon index.js

		/////////////////////////////////////////////////////

*/


// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require ("cors")

const userRoutes = require ("./routes/userRoutes");
const productRoutes = require ("./routes/productRoutes");

const app = express();

//Database connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.5e2pgug.mongodb.net/capstone2ecommerce?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

//Set notification for database connection
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"))
db.once("open", ()=> console.log("We are connected to cloud database"))

// Middlewares
app.use(cors());// must set cors first before anything else
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//defines the /users to be included for all user routes
app.use("/users", userRoutes);
//defines the /products to be included for all products routes
app.use("/products", productRoutes);

//


app.listen (process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})



