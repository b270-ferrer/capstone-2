const jwt = require("jsonwebtoken");
const secretKey = "Capstone2EcommerceB270";

// Token creation
// Steps:
// 	1. When the user logs in, a token will be created with the user's information payload 
// 	2. Generate a JWT using the jwt's sign method

		module.exports.createAccessToken = (user) => {
			const data = {
				id 			: 	user._id,
				name 		: 	user.name,				
				email 		: 	user.email,
				password 	: 	user.password,
				isAdmin 	: 	user.isAdmin
			};			
			return jwt.sign(data, secretKey, {});
		}

// Token verification
// Steps:
// 	1. If Token exist  remove "Bearer " prefix from the token string   
// 	2. Validates the token using the "verify" method decrypting the token using the secret code
// 	3. if validated, call next() to proceed to next middleware
//  4. return auth: "failed" if unsuccessful

		module.exports.verify = (req, res, next) => {
			let token = req.headers.authorization;
			if(token !== undefined) {
				token = token.slice(7, token.length);
				return jwt.verify(token, secretKey, (err, data) => {	
					(err) ? (res.send({auth: "failed"})) : next();		})	
			} else {
				return res.send({auth:"failed"});
			}
		}


// Token decryption
// Steps:
// 	1. If Token exist  remove "Bearer " prefix from the token string   
// 	2. Validates the token using the "verify" method decrypting the token using the secret code
// 	3. if validated, the jwt.decode() method will decode the complete JWT and return an object 
//		that includes the JWT header, payload, and signature
//  4. return null if unsuccessful
		module.exports.decode = (token) => {
			if(token !== undefined) {
				token = token.slice(7, token.length);
				return jwt.verify(token, secretKey, (err, data) => {	
					return ((err) ? null : jwt.decode(token, {complete:true}).payload)})
			} else {
				return null;
			}
		}
