const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require ("../auth")



///////////////////////////////////////////////////
//              SESSION 43                       //
//          - CREATE PRODUCT                     //
//          - RETRIEVE ALL ACTIVE PRODUCT        //
///////////////////////////////////////////////////

router.post("/create", auth.verify, productController.create)
router.get("/active", productController.getActive)
router.get("/all", productController.getAll)

///////////////////////////////////////////////////
//              SESSION 44                       //
//          - RETRIEVE SINGLE PRODUCT            //
//          - UPDATE PRODUCT INFORMATION(admin)  //
//          - ARCHIVE PRODUCT (admin)            //
///////////////////////////////////////////////////

router.get("/:productId", productController.getProduct);
router.put("/:productId", auth.verify, productController.updateProduct);
router.put("/:productId/archive", auth.verify, productController.archiveProduct);

module.exports = router;