const express = require("express");
const router = express.Router();
const userController = require ("../controllers/userController");
const auth = require ("../auth");


router.post("/register", userController.registerUser);
router.post("/register/admin", userController.registerAdmin);
router.post("/login", userController.loginUser);
router.post("/checkout", userController.createOrder);
//router.get("/:userId", userController.getUser);
router.get("/", userController.getUser);
router.put("/:userId/setAsAdmin", auth.verify, userController.setAdmin);
router.get("/:userId/myorders", auth.verify, userController.getOrder);
router.get("/order/all/:userId",  auth.verify, userController.getOrderAll);
router.put("/:userId/cart",  auth.verify, userController.addToCart);
router.delete("/:userId/cart/clear",  userController.clearCart);

module.exports = router;
