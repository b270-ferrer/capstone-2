const express = require("express");
const router = express.Router();
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

///////////////////////////////////////////////////
//              SESSION 43                       //
//          - CREATE PRODUCT (admin only)        //
//          - RETRIEVE ALL ACTIVE PRODUCT        //
///////////////////////////////////////////////////

// Create Product
// Steps:
// 	1. Create a new Product object using the mongoose model and the information from the request body
// 	2. Use auth.decode (req.headers.authorization) and check if the user is an Admin
// 	3. Save the new Product to the database

		module.exports.create = async (req,res) => {
			const userData = auth.decode (req.headers.authorization);	
			if (userData.isAdmin){
				let newProduct = new Product ({
					name 		: req.body.name,
					description	: req.body.description,
					price		: req.body.price,
					quantity	: req.body.quantity,
					imageUrl	: req.body.imageUrl	
				});
				return await newProduct.save()		
					.then (course => res.send (	console.log (course) || true ))				
					.catch (error => res.send ( console.log(error) || false));
			} else {
				res.send(false);
			}	
		}

// retrieve all ACTIVE product
		module.exports.getActive = async (req,res) => {
			return Product.find({isActive: true}).then( result=> res.send(result));
		}

// retrieve all  product (admin only)
		module.exports.getAll = async (req,res) => {
			const userData = auth.decode (req.headers.authorization);	
			if (userData.isAdmin){
				return Product.find().then( result=> res.send(result));
			}else{
				res.send(false);				
			}
		}
///////////////////////////////////////////////////
//              SESSION 44                       //
//          - RETRIEVE SINGLE PRODUCT            //
//          - UPDATE PRODUCT INFORMATION(admin)  //
//          - ARCHIVE PRODUCT (admin)            //
///////////////////////////////////////////////////

// Retrieving a specific product
//	Steps:
//	1. A Get request issend to /product/:productId endpoint.
//	2.	API retrives product tht matches productId URL paramter and returns it in its response

		module.exports.getProduct = async (req,res) => {			
			return Product.findById(req.params.productId)
				.then(result => res.send (console.log (result) || result))	
				.catch (error => res.send(console.log(error) || error));
		}

// UPDATE PRODUCT INFORMATION(admin)
//	Steps:
//	1. An authenticated admin user sends a PUT request containing a JWT in its header to /product/:productId endpoints.
//	2. API validates JWT. returns false if validated fails
//	3. If validates successful, API finds product with ID matching the productId URL parameter and overwrites its info with those from the request body		

		module.exports.updateProduct = async (req,res) => {
			const userData = auth.decode (req.headers.authorization);
			if (userData.isAdmin){	
				let updateProduct = {
					name 		: req.body.name,
					description	: req.body.description,
					price		: req.body.price,
					quantity	: req.body.quantity,
					imageUrl	: req.body.imageUrl		
				};
				return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true})
					.then(result => res.send ( true))
					.catch (error => res.send ( false));
			}else{		
				return res.send (`You dont have access to this page!`);
			}
		}


// ARCHIVE PRODUCT (admin)
//	Steps:
//	1. An authenticated admin user sends a PUT request contining a JWT in tis header to /product/:productID/archive endpoint
//	2. API validates JWT, return false if vlidate fails

		module.exports.archiveProduct = async (req,res) => {
			const userData = auth.decode (req.headers.authorization);
			if (userData.isAdmin){	
				let archiveProduct = {isActive: req.body.isActive};
				return await Product.findByIdAndUpdate(req.params.productId, archiveProduct, {new:true})
					.then (result => res.send ( true))
					.catch (error => res.send (false));
			}else{		
				return res.send (`You dont have access to this page!`);
			}
		}



