const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");




///////////////////////////////////////////////////
//              SESSION 42                       //
//          - DATA module                        //
//          - USER REGISTRATION                  //
//          - USER AUTHENTICATION                //
///////////////////////////////////////////////////


// User registration
//
// Steps:
// 	1. Create a new User object using the mongoose model and the information from the request body
// 	2. Make sure that the password is encrypted
// 	3. Save the new User to the database

		module.exports.registerUser = async (req, res) => {
			let newUser = new User({
				name 		: req.body.name,
				email 		: req.body.email,
				password 	: bcrypt.hashSync(req.body.password, 10),
				mobileNo 	: req.body.mobileNo
			})
			await newUser.save()
		  		.then(user => res.send(console.log(user) || true))
		  		.catch(error => res.send(console.log(error) || false));
		}

// Admin registration
//
// Steps:
// 	1. Create a new User object using the mongoose model and the information from the request body
// 	2. Make sure that the password is encrypted
// 	3. Set isAdmin = true
// 	4. Save the new Admin to the database

		module.exports.registerAdmin = async (req, res) => {
			let newAdmin = new User({
				name 		: req.body.name,
				email 		: req.body.email,
				password 	: bcrypt.hashSync(req.body.password, 10),
				mobileNo 	: req.body.mobileNo,
				isAdmin 	: true
			})
			await newAdmin.save()
		  		.then(user => res.send(console.log(user) || true))
		  		.catch(error => res.send(console.log(error) || false));
		}

// User authentication
//
// Steps:
// 	1. Check the database if the user email exists
// 	2. Compare the password provided in the login form with the password stored in the database
// 	3. Generate/return a JSON web token if the user is successfully logged in and return false if not

		module.exports.loginUser = async (req, res) => {
			return User.findOne({email: req.body.email}).then(result => {
				if(result == null) {
					return res.send({message: "No user found"});
				} else {
					const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
					return res.send(isPasswordCorrect ? {accessToken: auth.createAccessToken(result)} : false);
				}
			})
		};


///////////////////////////////////////////////////
//              SESSION 45                       //
//   - NON-ADMIN USER CHECKOUT (CREATE ORDER)    //
//   - RETRIEVE USER DETAIL                      //
//   - INITIAL DEPLOYMENT OF API TO RENDER       //
///////////////////////////////////////////////////


///////////////////////////////////////////////////
// NON-ADMIN USER CHECKOUT (CREATE ORDER)        //
//              STRETCH GOAL                     //
// 			ADD TO CART 						 //
//	- Added productS (via Postman)               //
//	- Change Product Quantities (via Postman)    //
//	- Remove Product From Cart (via Postman)     //
//	- Subtotal for each item                     //
//	- Total Price for each item                  //
///////////////////////////////////////////////////

		module.exports.createOrder = async (req, res) => {
			let userData = await User.findByIdAndUpdate(req.body.userId)
					.catch(error => console.log(`Product Data Saving FAILED: ${error}`));
			if (!userData.isAdmin){	
				let productList = [];
				let totalAmount = 0;
				for (let orderProduct of req.body.products) {
		  			let productData = 		await Product.findById(orderProduct.productId);
		  			productData.quantity -= orderProduct.quantity
		  			if (await productData.save()
				 		.then (product => (console.log(`Product Data Saving SUCCESSFUL`) || true ) )
				 		.catch(error => (console.log(`Product Data Saving FAILED: ${error}`) || false ) )
				 	){
						productList.push({
				    		id: 		orderProduct.productId,
				    		name: 		productData.name,
				    		price: 		productData.price,
				    		quantity: 	orderProduct.quantity,
				    		total:    	(productData.price * orderProduct.quantity)		
			  			});
						totalAmount += (productData.price * orderProduct.quantity);
		  			}	
				}
				let newOrder = new Order({		
					userId: 			req.body.userId,
					userName: 			userData.name , 
					products: 			productList, 
					totalAmount: 		totalAmount
				});

				await newOrder.save()
				  	.then(order => (console.log(`Order Data Saving SUCCESSFUL `) || true))
				   	.catch(error => (console.log(`Order  Data Saving FAILED ${error}`) || false));


				userData.orders.push({orderId: newOrder._id});

				console.log (userData.orders)

				await userData.save()
				  	.then(user => (console.log(`User Data Saving SUCCESSFUL `) || true))
				   	.catch(error => (console.log(`User  Data Saving FAILED ${error}`) || false));

				return res.send(true);	
			}else{
				return res.send(false);		
			}
		}

//RETRIEVE USER DETAIL
		module.exports.getUser = async (req,res) => {	
			const userData = auth.decode (req.headers.authorization);///		
			//return User.findById(req.params.userId)
			return User.findById(userData.id)
				.then(result => res.send (console.log (result) || result))	
				.catch (error => res.send(console.log(error) || error));
		}

///////////////////////////////////////////////////
//              STRETCH GOAL                     //
//   - SET USER AS ADMIN (ADMIN ONLY)            //
//   - RETRIEVE AUTHENTICATED USER ORDER         //
//   - RETRIEVE ALL ORDER (ADMIN ONLY) 			 //	
///////////////////////////////////////////////////

// SET USER AS ADMIN (ADMIN ONLY) 
		module.exports.setAdmin = (req,res) => {
			const userData = auth.decode (req.headers.authorization);
			if (userData.isAdmin){	
				let userAdmin = {isAdmin: req.body.isAdmin};		
				return User.findByIdAndUpdate(req.params.userId, userAdmin)
					.then(result => res.send (true))		
					.catch (error => res.send (console.log(error) || false));			
			}else{		
				return res.send(`You dont have access to this page!`);
			}
		}

//  RETRIEVE AUTHENTICATED USER ORDER 

		module.exports.getOrder = (req,res) => {
			const userData = auth.decode (req.headers.authorization);
			if (!userData.isAdmin)	
				return Order.find({userId: userData.id}).then( result=> res.send(result)); 
			return res.send(false);
		};



// RETRIEVE ALL ORDER (ADMIN ONLY) 



		module.exports.getOrderAll = (req,res) => {
			const userData = auth.decode (req.headers.authorization);
			if (userData.isAdmin){					
				return Order.find({})
					.then( result=> res.send(result))
		 			.catch (error => res.send (console.log(error)));
		 	}else{		
				return res.send(`You dont have access to this page!`);
			}
		}
		

// ADD TO CART

		module.exports.addToCart = async (req, res) => {
			let userData = await User.findByIdAndUpdate(req.body.userId)
			//let userData = auth.decode (req.headers.authorization)
				.catch(error => console.log(`Product Data Saving FAILED: ${error}`));
			if (!userData.isAdmin){	
				let productList = [];
				let totalAmount = 0;
				for (let orderProduct of req.body.products) {
		  			let productData = 		await Product.findById(orderProduct.productId);
		  			productData.quantity -= orderProduct.quantity
		  			if (await productData.save()
				 		//.then (product => (console.log(`Product Data Saving SUCCESSFUL ${product}`) || true ) )
				 		.then (product => ( true ) )
				 		.catch(error => (console.log(`Product Data Saving FAILED: ${error}`) || false ) )
				 	){
						productList.push({
				    		id: 		orderProduct.productId,
				    		name: 		productData.name,
				    		price: 		productData.price,
				    		quantity: 	orderProduct.quantity,
				    		total:    	(productData.price * orderProduct.quantity)		
			  			});
						totalAmount += (productData.price * orderProduct.quantity);
		  			}	
				}
				userData.cart.products = productList;
				userData.cart.totalAmount = totalAmount;
				await userData.save()
				  	.then(user => (console.log(`User Data Saving SUCCESSFUL ${user}`) || true))
				   	.catch(error => (console.log(`User  Data Saving FAILED ${error}`) || false));

				return res.send(true);	
			}else{
				return res.send(false);		
			}
		}
// CLEAR CART

		module.exports.clearCart = async (req, res) => {
			//let userData = await User.findByIdAndUpdate(req.body.userId)
			let userData = await User.findById(req.body.userId)
				.catch(error => console.log(`Product Data Saving FAILED: ${error}`));			
			userData.cart.products = [];
			userData.cart.totalAmount = 0;
			await userData.save()
				 .then(user => res.send(console.log(`User Data Saving SUCCESSFUL`) || true))
				 .catch(error => res.send(console.log(`User  Data Saving FAILED ${error}`) || false));		
		}